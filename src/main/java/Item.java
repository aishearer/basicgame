/**
 * Created by Andrew on 6/26/2016.
 */
public abstract class Item {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
